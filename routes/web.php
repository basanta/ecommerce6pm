<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});


Route::get('/category', function () {
    return view('frontend.category');
});

//Route::get('/admin', function () {
//    return view('backend.index');
//});

Route::get('/admin', 'Backend\DashboardController@dashboard')->name('admin.dashboard');


Route::get('/login', function () {
    return redirect('/admin/login');
});

Route::get('/admin/login', 'Backend\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Backend\AdminLoginController@login')->name('admin.dologin');

Route::get('/admin/logout', 'Backend\AdminLoginController@logout')->name('admin.logout');
