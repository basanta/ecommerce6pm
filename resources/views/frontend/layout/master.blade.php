<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Super Market an Ecommerce Online Shopping Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    @include('frontend.includes.style')
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="{{asset('assets/frontend/js/jquery-1.11.1.min.js')}}"></script>
    <!-- //js -->
    <link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="{{asset('assets/frontend/js/move-top.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/js/easing.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
</head>

<body>
<!-- header -->
<div class="agileits_header">
    <div class="container">
        <div class="w3l_offers">
            <p>SALE UP TO 70% OFF. USE CODE "SALE70%" . <a href="products.html">SHOP NOW</a></p>
        </div>
        <div class="agile-login">
            <ul>
                <li><a href="{{asset('assets/frontend/')}}registered.html"> Create Account </a></li>
                <li><a href="{{asset('assets/frontend/')}}login.html">Login</a></li>
                <li><a href="{{asset('assets/frontend/')}}contact.html">Help</a></li>

            </ul>
        </div>
        <div class="product_list_header">
            <form action="#" method="post" class="last">
                <input type="hidden" name="cmd" value="_cart">
                <input type="hidden" name="display" value="1">
                <button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
            </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<div class="logo_products">
    <div class="container">
        <div class="w3ls_logo_products_left1">
            <ul class="phone_email">
                <li><i class="fa fa-phone" aria-hidden="true"></i>Order online or call us : (+0123) 234 567</li>

            </ul>
        </div>
        <div class="w3ls_logo_products_left">
            <h1><a href="{{asset('assets/frontend/')}}index.html">super Market</a></h1>
        </div>
        <div class="w3l_search">
            <form action="#" method="post">
                <input type="search" name="Search" placeholder="Search for a Product..." required="">
                <button type="submit" class="btn btn-default search" aria-label="Left Align">
                    <i class="fa fa-search" aria-hidden="true"> </i>
                </button>
                <div class="clearfix"></div>
            </form>
        </div>

        <div class="clearfix"> </div>
    </div>
</div>
<!-- //header -->
<!-- navigation -->
<div class="navigation-agileits">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{asset('assets/frontend/')}}index.html" class="act">Home</a></li>
                    <!-- Mega Menu -->
                    <li class="dropdown">
                        <a href="{{asset('assets/frontend/')}}#" class="dropdown-toggle" data-toggle="dropdown">Groceries<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>All Groceries</h6>
                                        <li><a href="{{asset('assets/frontend/')}}groceries.html">Dals & Pulses</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}groceries.html">Almonds</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}groceries.html">Cashews</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}groceries.html">Dry Fruit</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}groceries.html"> Mukhwas </a></li>
                                        <li><a href="{{asset('assets/frontend/')}}groceries.html">Rice & Rice Products</a></li>
                                    </ul>
                                </div>

                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="{{asset('assets/frontend/')}}#" class="dropdown-toggle" data-toggle="dropdown">Household<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>All Household</h6>
                                        <li><a href="{{asset('assets/frontend/')}}household.html">Cookware</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}household.html">Dust Pans</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}household.html">Scrubbers</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}household.html">Dust Cloth</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}household.html"> Mops </a></li>
                                        <li><a href="{{asset('assets/frontend/')}}household.html">Kitchenware</a></li>
                                    </ul>
                                </div>


                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="{{asset('assets/frontend/')}}#" class="dropdown-toggle" data-toggle="dropdown">Personal Care<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Baby Care</h6>
                                        <li><a href="{{asset('assets/frontend/')}}personalcare.html">Baby Soap</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}personalcare.html">Baby Care Accessories</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}personalcare.html">Baby Oil & Shampoos</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}personalcare.html">Baby Creams & Lotion</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}personalcare.html"> Baby Powder</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}personalcare.html">Diapers & Wipes</a></li>
                                    </ul>
                                </div>

                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="{{asset('assets/frontend/')}}#" class="dropdown-toggle" data-toggle="dropdown">Packaged Foods<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>All Accessories</h6>
                                        <li><a href="{{asset('assets/frontend/')}}packagedfoods.html">Baby Food</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}packagedfoods.html">Dessert Items</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}packagedfoods.html">Biscuits</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}packagedfoods.html">Breakfast Cereals</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}packagedfoods.html"> Canned Food </a></li>
                                        <li><a href="{{asset('assets/frontend/')}}packagedfoods.html">Chocolates & Sweets</a></li>
                                    </ul>
                                </div>


                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="{{asset('assets/frontend/')}}#" class="dropdown-toggle" data-toggle="dropdown">Beverages<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Tea & Coeffe</h6>
                                        <li><a href="{{asset('assets/frontend/')}}beverages.html">Green Tea</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}beverages.html">Ground Coffee</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}beverages.html">Herbal Tea</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}beverages.html">Instant Coffee</a></li>
                                        <li><a href="{{asset('assets/frontend/')}}beverages.html"> Tea </a></li>
                                        <li><a href="{{asset('assets/frontend/')}}beverages.html">Tea Bags</a></li>
                                    </ul>
                                </div>

                            </div>
                        </ul>
                    </li>
                    <li><a href="{{asset('assets/frontend/')}}gourmet.html">Gourmet</a></li>
                    <li><a href="{{asset('assets/frontend/')}}offers.html">Offers</a></li>
                    <li><a href="{{asset('assets/frontend/')}}contact.html">Contact</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>

<!-- //navigation -->
@yield('content')
<!-- //footer -->
<div class="footer">
    <div class="container">
        <div class="w3_footer_grids">
            <div class="col-md-3 w3_footer_grid">
                <h3>Contact</h3>

                <ul class="address">
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>1234k Avenue, 4th block, <span>New York City.</span></li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}mailto:info@example.com">info@example.com</a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>+1234 567 567</li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Information</h3>
                <ul class="info">
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}about.html">About Us</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}contact.html">Contact Us</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}short-codes.html">Short Codes</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}faq.html">FAQ's</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}products.html">Special Products</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Category</h3>
                <ul class="info">
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}groceries.html">Groceries</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}household.html">Household</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}personalcare.html">Personal Care</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}packagedfoods.html">Packaged Foods</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}beverages.html">Beverages</a></li>
                </ul>
            </div>
            <div class="col-md-3 w3_footer_grid">
                <h3>Profile</h3>
                <ul class="info">
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}products.html">Store</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}checkout.html">My Cart</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}login.html">Login</a></li>
                    <li><i class="fa fa-arrow-right" aria-hidden="true"></i><a href="{{asset('assets/frontend/')}}registered.html">Create Account</a></li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="footer-copy">

        <div class="container">
            <p>© 2017 Super Market. All rights reserved | Design by <a href="{{asset('assets/frontend/')}}http://w3layouts.com/">W3layouts</a></p>
        </div>
    </div>

</div>
<div class="footer-botm">
    <div class="container">
        <div class="w3layouts-foot">
            <ul>
                <li><a href="{{asset('assets/frontend/')}}#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="{{asset('assets/frontend/')}}#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="{{asset('assets/frontend/')}}#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                <li><a href="{{asset('assets/frontend/')}}#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="payment-w3ls">
            <img src="images/card.png" alt=" " class="img-responsive">
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //footer -->
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>

<!-- top-header and slider -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
<script src="{{asset('assets/frontend/js/minicart.min.js')}}"></script>
<script>
    // Mini Cart
    paypal.minicart.render({
        action: '#'
    });

    if (~window.location.search.indexOf('reset=true')) {
        paypal.minicart.reset();
    }
</script>
<!-- main slider-banner -->
<script src="{{asset('assets/frontend/js/skdslider.min.js')}}"></script>
<link href="{{asset('assets/frontend/css/skdslider.css')}}" rel="stylesheet">
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});

        jQuery('#responsive').change(function(){
            $('#responsive_wrapper').width(jQuery(this).val());
        });

    });
</script>
<!-- //main slider-banner -->
</body>
</html>