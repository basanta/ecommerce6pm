<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    function  dashboard(){
        return view('backend.index');
    }

    function __construct ()
    {
        if(Auth::check())
        {
            return redirect('/admin/login');

        }
    }
    protected $loginPath = '/admin/login';



}
